const Logo = () =>
  <a href="#" className="header__menu-logo">
    <img src="https://i.pinimg.com/originals/2e/e7/0f/2ee70fdf94cc1e52b17fa738f1e4fd62.png" alt="" />
  </a>

const navMenu = ({ url = '#', children, className = "header__menu-item" }) =>
  <li>
    <a href={url} className="header__menu-link">
      {children}
    </a>
  </li>

const Header = () => {
  return (
    <header className="header">
      <nav className="header__menu">
        <Logo />
        <ul className="header__menu-list">
          <navMenu url="#" className="header__menu-item">Home</navMenu>
          <navMenu url="#" className="header__menu-item">About us</navMenu>
          <navMenu url="#" className="header__menu-item">Menu</navMenu>
          <navMenu url="#" className="header__menu-item">Contact us</navMenu>
        </ul>
      </nav>
    </header>
  );
}



export default Header;
