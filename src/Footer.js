const Logo = () =>
  <a href="#" className="header__menu-logo">
    <img src="https://i.pinimg.com/originals/2e/e7/0f/2ee70fdf94cc1e52b17fa738f1e4fd62.png" alt="" />
  </a>

const footerListItem = ({ url = '#', children, className = "footer__content-item" }) =>
  <li>
    <a href={url} class="footer__content-link">
      {children}
    </a>
  </li>

const footerListTitle = ({ url = '#', children, className = "footer__content-title" }) =>
  <h6>
    {children}
  </h6>

const footerListSocItem = ({ url = '#', children, className = "footer__content-soc__item" }) =>
  <li>
    <a href={url} className="footer__content-link">
      <img src={children} alt="" className="social_icon" alt="" />
    </a>
  </li>

const Footer = () => {
  return (  
      <footer className="footer">       
        <div className="footer__inner">
          <Logo />
          <div className="footer__content">
            <ul className="footer__content-list">
              <footerListItem url="#" className="footer__content-item">
                  companyname@gmail.com
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Phone: +7485--118--03--25
              </footerListItem>
            </ul>
          </div>
          <div className="footer__content">
            <footerListTitle url="#" className="footer__content-title">
                Home
            </footerListTitle>
            <ul className="footer__content-list">
              <footerListItem url="#" className="footer__content-item">
                  Landingpage
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Referral Program
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  UI & UX Design
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Web Design
              </footerListItem>
            </ul>
          </div>
          <div className="footer__content">
            <footerListTitle url="#" className="footer__content-title">
                Menu
            </footerListTitle>
            <ul className="footer__content-list">
              <footerListItem url="#" className="footer__content-item">
                  Documentation
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Referral Program
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  UI & UX Design
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Web Design
              </footerListItem>
            </ul>
          </div>
          <div className="footer__content">
            <footerListTitle url="#" className="footer__content-title">
                Company
            </footerListTitle>
            <ul className="footer__content-list">
              <footerListItem url="#" className="footer__content-item">
                  Landingpage
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Documentation
              </footerListItem>
              <footerListItem url="#" className="footer__content-item">
                  Referral Program
              </footerListItem>
            </ul>
            <ul className="footer__content-soc__list">
            <footerListSocItem><img src="/images/footer-soc-1.svg" /></footerListSocItem>
              <footerListSocItem><img src="/images/footer-soc-2.svg" /></footerListSocItem>
              <footerListSocItem><img src="/images/footer-soc-3.svg" /></footerListSocItem>
              <footerListSocItem><img src="/images/footer-soc-4.svg" /></footerListSocItem>
            </ul>
          </div>
        </div>
    </footer>
  );
}


export default Footer;
