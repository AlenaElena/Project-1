
const Menu = () => {
  return (
      <section class="menu">
        <div class="caption">
          <h4 class="section__subtitle">Features</h4>
          <h2 class="section__title">Food with a New Passion</h2>
          <p class="section__text">
              There are many things are needed to start the Fast Food Business. You need not only Just Food Stalls with
              Persons but
              also specialized equipment, Skills to manage Customers,
          </p>
        </div>
        <div class="menu__cards">
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                    Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                    16$
                </h3>
              </div>
              <p class="menu-card__text">
                  There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                  Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                  16$
                </h3>
              </div>
              <p class="menu-card__text">
                There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                  Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                  16$
                </h3>
              </div>
              <p class="menu-card__text">
                There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                  Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                  16$
                </h3>
              </div>
              <p class="menu-card__text">
                There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                  Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                  16$
                </h3>
              </div>
              <p class="menu-card__text">
                There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
          <div class="menu-card">
            <img src="https://img.cppng.com/download/2020-06/6-2-burger-png-image.png" alt="" class="menu-card__img" />
            <div class="menu-card__content">
              <div class="menu-card__top">
                <h3 class="menu-card__top-name">
                  Vegie Muffen
                </h3>
                <h3 class="menu-card__top-cost">
                  16$
                </h3>
              </div>
              <p class="menu-card__text">
                There are many things are needed to start the Fast Food Business.
              </p>
              <div class="menu-card__order">
                <button class="menu-card__order-btn" type="submit">
                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.74868 6.74868V12H5.25132V6.74868H0V5.25132H5.25132V0H6.74868V5.25132H12V6.74868H6.74868Z" fill="white" /></svg>
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
  );
}






export default Menu;
