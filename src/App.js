import React from 'react';

import './App.css';
import { Children } from 'react';
import Header from './Header';
import Footer from './Footer';
import Menu from './Menu';


const App = () => {
  return (
    <div className="app-burger">
      <Header />
      <Menu />
      <Footer />
    </div>
  );
}






export default App;
